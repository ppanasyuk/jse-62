package ru.t1.panasyuk.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.component.Bootstrap;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;

public final class Application {

    public static void main(final String... args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}