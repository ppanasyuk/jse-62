package ru.t1.panasyuk.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import java.util.Collection;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> {

    M add(@Nullable M model);

    void clear(@NotNull String userId);

    void clear();

    boolean existsById(@NotNull String userId, @Nullable String id);

    List<M> findAll();

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @Nullable Integer index);

    long getSize(@NotNull String userId);

    @Nullable
    M removeById(@NotNull String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void update(@NotNull M model);

}