package ru.t1.panasyuk.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User add(@NotNull User user);

    void clear();

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable String email, @Nullable Role role);

    boolean existsById(@Nullable String id);

    List<User> findAll();

    User findByLogin(@Nullable String login);

    User findByEmail(@Nullable String email);

    User findOneById(@Nullable String id);

    User findOneByIndex(@Nullable Integer index);

    long getSize();

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    User removeById(@Nullable String id);

    void lockUserByLogin(@Nullable String login);

    @Nullable
    User remove(@NotNull User user);

    @Nullable
    User removeByIndex(@Nullable Integer index);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByEmail(@Nullable String email);

    Collection<User> set(@NotNull Collection<User> users);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    void unlockUserByLogin(@Nullable String login);

    void update(@NotNull User user);

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

}