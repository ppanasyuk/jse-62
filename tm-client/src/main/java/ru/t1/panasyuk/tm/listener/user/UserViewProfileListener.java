package ru.t1.panasyuk.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.panasyuk.tm.dto.response.user.UserViewProfileResponse;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    private final String NAME = "user-view-profile";

    @NotNull
    private final String DESCRIPTION = "View profile of current user.";

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @NotNull final UserViewProfileResponse response = authEndpoint.viewUserProfile(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
        System.out.println(response.getUser());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}