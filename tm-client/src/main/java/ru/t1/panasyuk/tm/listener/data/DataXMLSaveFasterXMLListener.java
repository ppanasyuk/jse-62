package ru.t1.panasyuk.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataXMLSaveFasterXMLRequest;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class DataXMLSaveFasterXMLListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file.";

    @NotNull
    private static final String NAME = "data-save-xml-fasterxml";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXMLSaveFasterXMLListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXMLSaveFasterXMLRequest request = new DataXMLSaveFasterXMLRequest(getToken());
        domainEndpoint.saveDataXMLFasterXML(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
